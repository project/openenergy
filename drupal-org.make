; open_energy make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0-alpha2"
projects[module_filter][subdir] = "contrib"

projects[bacnet][version] = "1.0"
projects[bacnet][subdir] = "contrib"

projects[ctools][version] = "1.5"
projects[ctools][subdir] = "contrib"

projects[bueditor][version] = "1.8"
projects[bueditor][subdir] = "contrib"

projects[markdowneditor][version] = "1.4"
projects[markdowneditor][subdir] = "contrib"

projects[context][version] = "3.3"
projects[context][subdir] = "contrib"

projects[dkan_dataset][version] = "1.5"
projects[dkan_dataset][subdir] = "contrib"

projects[dkan_datastore][version] = "1.5"
projects[dkan_datastore][subdir] = "contrib"

projects[dkan_dataset][version] = "1.5"
projects[dkan_dataset][subdir] = "contrib"

projects[dkan_datastore][version] = "1.5"
projects[dkan_datastore][subdir] = "contrib"

projects[dkan_dataset][version] = "1.5"
projects[dkan_dataset][subdir] = "contrib"

projects[data][version] = "1.x-dev"
projects[data][subdir] = "contrib"

projects[date][version] = "2.8"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[features][version] = "2.2"
projects[features][subdir] = "contrib"

projects[feeds][version] = "2.x-dev"
projects[feeds][subdir] = "contrib"

projects[feeds_field_fetcher][version] = "mx-dev"
projects[feeds_field_fetcher][subdir] = "contrib"

projects[double_field][version] = "2.3"
projects[double_field][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[field_group][version] = "1.4"
projects[field_group][subdir] = "contrib"

projects[filefield_sources][version] = "1.9"
projects[filefield_sources][subdir] = "contrib"

projects[geofield][version] = "1.2"
projects[geofield][subdir] = "contrib"

projects[link][version] = "1.3"
projects[link][subdir] = "contrib"

projects[link_iframe_formatter][version] = "1.1"
projects[link_iframe_formatter][subdir] = "contrib"

projects[multistep][version] = "1.x-dev"
projects[multistep][subdir] = "contrib"

projects[ref_field][version] = "2.0-alpha1"
projects[ref_field][subdir] = "contrib"

projects[remote_file_source][version] = "1.0"
projects[remote_file_source][subdir] = "contrib"

projects[select_or_other][version] = "2.20"
projects[select_or_other][subdir] = "contrib"

projects[markdown][version] = "1.2"
projects[markdown][subdir] = "contrib"

projects[imagecache_actions][version] = "1.x-dev"
projects[imagecache_actions][subdir] = "contrib"

projects[og][version] = "2.7"
projects[og][subdir] = "contrib"

projects[og_extras][version] = "1.1"
projects[og_extras][subdir] = "contrib"

projects[og_moderation][version] = "2.3"
projects[og_moderation][subdir] = "contrib"

projects[colorizer][version] = "1.4"
projects[colorizer][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[entity][version] = "1.x-dev"
projects[entity][subdir] = "contrib"

projects[entity_rdf][version] = "1.x-dev"
projects[entity_rdf][subdir] = "contrib"

projects[geophp][version] = "1.7"
projects[geophp][subdir] = "contrib"

projects[gravatar][version] = "1.1"
projects[gravatar][subdir] = "contrib"

projects[job_scheduler][version] = "2.x-dev"
projects[job_scheduler][subdir] = "contrib"

projects[leaflet_widget][version] = "1.0-beta2"
projects[leaflet_widget][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[r4032login][version] = "1.8"
projects[r4032login][subdir] = "contrib"

projects[remote_stream_wrapper][version] = "1.0-rc1"
projects[remote_stream_wrapper][subdir] = "contrib"

projects[restws][version] = "2.2"
projects[restws][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.5"
projects[token][subdir] = "contrib"

projects[rdfx][version] = "2.x-dev"
projects[rdfx][subdir] = "contrib"

projects[rules][version] = "2.7"
projects[rules][subdir] = "contrib"

projects[search_api][version] = "1.13"
projects[search_api][subdir] = "contrib"

projects[search_api_db][version] = "1.4"
projects[search_api_db][subdir] = "contrib"

projects[facetapi][version] = "1.5"
projects[facetapi][subdir] = "contrib"

projects[facetapi_pretty_paths][version] = "1.1"
projects[facetapi_pretty_paths][subdir] = "contrib"

projects[services][version] = "3.10"
projects[services][subdir] = "contrib"



projects[delta][version] = "3.0-beta11"
projects[delta][subdir] = "contrib"

projects[uuid][version] = "1.0-alpha6"
projects[uuid][subdir] = "contrib"

projects[autocomplete_deluxe][version] = "2.0-beta3"
projects[autocomplete_deluxe][subdir] = "contrib"

projects[beautytips][version] = "2.x-dev"
projects[beautytips][subdir] = "contrib"

projects[chosen][version] = "2.0-beta4"
projects[chosen][subdir] = "contrib"

projects[jquery_update][version] = "2.4"
projects[jquery_update][subdir] = "contrib"

projects[eva][version] = "1.2"
projects[eva][subdir] = "contrib"

projects[views][version] = "3.8"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.2"
projects[views_bulk_operations][subdir] = "contrib"

projects[views_responsive_grid][version] = "1.3"
projects[views_responsive_grid][subdir] = "contrib"

; DKAN
projects[dkan_dataset][subdir] = dkan
projects[dkan_dataset][download][type] = git
projects[dkan_dataset][download][url] = https://github.com/NuCivic/dkan_dataset.git
projects[dkan_dataset][download][branch] = 7.x-1.x

projects[dkan_datastore][subdir] = dkan
projects[dkan_datastore][download][type] = git
projects[dkan_datastore][download][url] = https://github.com/NuCivic/dkan_datastore.git
projects[dkan_datastore][download][branch] = 7.x-1.x

includes[dkan_dataset_make] = https://raw.githubusercontent.com/NuCivic/dkan_dataset/7.x-1.x/dkan_dataset.make
includes[dkan_datastore_make] = https://raw.githubusercontent.com/NuCivic/dkan_datastore/7.x-1.x/dkan_datastore.make

projects[field_group_table][download][type] = git
projects[field_group_table][download][url] = "https://github.com/nuams/field_group_table.git"
projects[field_group_table][subdir] = contrib
projects[field_group_table][type] = module

projects[recline][download][type] = git
projects[recline][download][url] = https://github.com/NuCivic/recline.git
projects[recline][download][branch] = 7.x-1.x
projects[recline][subdir] = contrib

; +++++ Themes +++++

; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3.0"
projects[bootstrap][subdir] = "contrib"

; energy_dashboard
projects[energy_dashboard][type] = "theme"
projects[energy_dashboard][download][type] = "git"
projects[energy_dashboard][download][url] = "http://git.drupal.org/sandbox/cfox612/2363125.git"
projects[energy_dashboard][download][branch] = "7.x-1.x"
projects[energy_dashboard][download][revision] = "302fbbaa23b8e28a01c9eef7eb54ef13fd77e69e"

